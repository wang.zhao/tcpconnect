﻿
namespace Client
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.dataGrid = new System.Windows.Forms.DataGridView();
			this.btnLoadDS = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.label1.ForeColor = System.Drawing.Color.Red;
			this.label1.Location = new System.Drawing.Point(195, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(183, 50);
			this.label1.TabIndex = 0;
			this.label1.Text = "CLIENT UI";
			// 
			// dataGrid
			// 
			this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGrid.Location = new System.Drawing.Point(12, 101);
			this.dataGrid.Name = "dataGrid";
			this.dataGrid.RowTemplate.Height = 25;
			this.dataGrid.Size = new System.Drawing.Size(531, 236);
			this.dataGrid.TabIndex = 1;
			// 
			// btnLoadDS
			// 
			this.btnLoadDS.Location = new System.Drawing.Point(395, 72);
			this.btnLoadDS.Name = "btnLoadDS";
			this.btnLoadDS.Size = new System.Drawing.Size(148, 23);
			this.btnLoadDS.TabIndex = 2;
			this.btnLoadDS.Text = "Lấy Danh Sách File";
			this.btnLoadDS.UseVisualStyleBackColor = true;
			this.btnLoadDS.Click += new System.EventHandler(this.btnLoadDS_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(555, 349);
			this.Controls.Add(this.btnLoadDS);
			this.Controls.Add(this.dataGrid);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView dataGrid;
		private System.Windows.Forms.Button btnLoadDS;
	}
}

