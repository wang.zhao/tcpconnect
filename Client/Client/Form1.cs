﻿using System;
using System.Data;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Text;
using System.Windows.Forms;

namespace Client
{
	public partial class Form1 : Form
	{
		private const int BUFFER_SIZE = 1024;
		private const int PORT_NUMBER = 9999;

		static ASCIIEncoding encoding = new ASCIIEncoding();

		TcpClient client = new TcpClient();

		public Form1()
		{
			InitializeComponent();

			// 1. connect
			client.Connect("127.0.0.1", PORT_NUMBER);
			
		}

		private void btnLoadDS_Click(object sender, EventArgs e)
		{
			try
			{


				Stream stream = client.GetStream();

				Console.WriteLine("Connected to Server.");
				Console.Write("Enter your name: ");

				string str = "GetDataList";

				var reader = new StreamReader(stream);
				var writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				writer.WriteLine(str);

				string result = reader.ReadLine();

				MessageBox.Show(result);
			}

			catch (Exception ex)
			{
				Console.WriteLine("Error: " + ex);
			}

			Console.Read();
		}
	}
}
